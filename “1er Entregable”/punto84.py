#escribir un programa que lea un archivo Excel y rellene las celdas vacías de una columna con el valor promedio de la columna  
import pandas as pd

archivo_excel = "archivo_excel.xlsx"
df = pd.read_excel(archivo_excel)

SRQ_columna = "productos" 

SRQ_valor_promedio = df[SRQ_columna].mean()

df[SRQ_columna] = df[SRQ_columna].fillna()

archivo_excel_actualizado = "archivo_excel_actualizado.xlsx"
df.to_excel(archivo_excel_actualizado, index=False)

print(f"Se ha actualizado el archivo Excel '{archivo_excel}' y se ha guardado como '{archivo_excel_actualizado}'.")

