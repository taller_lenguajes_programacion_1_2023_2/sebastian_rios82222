#simula un archivo CSV productos.csv escribir un programa que filtre y muestre 
#aquellos porductos cuyo precio sea mayor  a 50

import csv
archivo_csv = "productos.csv"
SRQ_productos_filtrados = []
with open(archivo_csv, mode='r') as file:
    reader = csv.DictReader(file)
    for row in reader:
        nombre = row["Nombre"]
        precio = float(row["Precio"])
        if precio > 50:
            SRQ_productos_filtrados.append({"Nombre": nombre, "Precio": precio})

print("Productos con precio mayor a 50:")
for producto in SRQ_productos_filtrados:
    print(f"Nombre: {producto['Nombre']}, Precio: {producto['Precio']}")


