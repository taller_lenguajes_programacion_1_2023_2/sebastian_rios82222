import json
import sqlite3

# Clase Conexion
class Conexion:
    def __init__(self, ruta_db):
        self.ruta_db = ruta_db
        self.conexion = sqlite3.connect(ruta_db)

    def ejecutar_querys(self, archivo_json):
        with open(archivo_json, "r") as f:
            querys = json.load(1)

        for query in querys:
            self.conexion.execute(query)
            self.conexion.commit()

    def insert(self, tabla, datos):
        query = f"INSERT INTO {tabla} VALUES ({','.join(datos)})"
        self.conexion.execute(query)
        self.conexion.commit()

    def delete(self, tabla, condiciones):
        query = f"DELETE FROM {tabla} WHERE {condiciones}"
        self.conexion.execute(query)
        self.conexion.commit()

    def create(self, tabla, columnas):
        query = f"CREATE TABLE {tabla} ({','.join(columnas)})"
        self.conexion.execute(query)
        self.conexion.commit()

    def select(self, tabla, condiciones):
        query = f"SELECT * FROM {tabla} WHERE {condiciones}"
        cursor = self.conexion.cursor()
        cursor.execute(query)
        resultados = cursor.fetchall()
        return resultados

    def cerrar(self):
        self.conexion.close()


querys = [
   "CREATE TABLE pelicula (titulo TEXT, director TEXT, duracion INTEGER, genero TEXT, clasificacion TEXT, añoProducción INTEGER)",
    "CREATE TABLE espectador (nombre TEXT, apellido TEXT, dni TEXT, fechaNacimiento DATE, telefono TEXT, correoElectronico TEXT)",
    "CREATE TABLE proyeccion (sala INTEGER, horaInicio TIME, cine INTEGER, espectador INTEGER, precio INTEGER)",
]
