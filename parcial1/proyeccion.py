  from cine import cine
class Proyeccion(Cine):
    def __init__(self, sala, horaInicio, cine, espectador, precio):
        super().__init__(cine.nombre, cine.direccion, cine.capacidadTotal, cine.numeroSalas, cine.telefono, cine.correoElectronico)
        self.sala = sala
        self.horaInicio = horaInicio
        self.espectador = espectador
        self.precio = precio


    @property
    def sala(self):
        return self._sala

    @sala.setter
    def sala(self, sala):
        self._sala = sala

    @property
    def horaInicio(self):
        return self._horaInicio

    @horaInicio.setter
    def horaInicio(self, horaInicio):
        self._horaInicio = horaInicio

    @property
    def espectador(self):
        return self._espectador

    @espectador.setter
    def espectador(self, espectador):
        self._espectador = espectador

    @property
    def precio(self):
        return self._precio

    @precio.setter
    def precio(self, precio):
        self._precio = precio

    def __str__(self):
        return f"{self.sala} - {self.horaInicio} - {self.cine.nombre} - {self.espectador.nombre} - {self.precio}"