class cine:
    
    def __init__(self, nombre, direccion, capacidadTotal, numeroSalas, telefono, correoElectronico):
        self.nombre = nombre
        self.direccion = direccion
        self.capacidadTotal = capacidadTotal
        self.numeroSalas = numeroSalas
        self.telefono = telefono
        self.correoElectronico = correoElectronico

        
    def __str__(self):
        return f"{self.nombre} - {self.direccion}"
