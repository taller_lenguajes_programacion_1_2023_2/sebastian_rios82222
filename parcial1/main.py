import sqlite3
import json
# Importa las clases
from cine import cine
from pelicula import pelicula
from espectador import espectador
from proyeccion import Proyeccion
from conexion import Conexion

# Archivo JSON con los querys
querys = [
    "CREATE TABLE pelicula (titulo TEXT, director TEXT, duracion INTEGER, genero TEXT, clasificacion TEXT, añoProducción INTEGER)",
    "CREATE TABLE espectador (nombre TEXT, apellido TEXT, dni TEXT, fechaNacimiento DATE, telefono TEXT, correoElectronico TEXT)",
    "CREATE TABLE proyeccion (sala INTEGER, horaInicio TIME, cine INTEGER, espectador INTEGER, precio INTEGER)",
]

# Crea un objeto de la clase Conexion
conexion = Conexion("cine.db")

# Ejecuta los querys especificados en el diagrama de clases
conexion.ejecutar_querys(querys = json.load(1))

# Crea una instancia de la clase Cine
cine = cine("Cinemark", "Calle 100, Bogotá", 100, 5, "310 555 5555", "cinemark@example.com")

# Crea una instancia de la clase Pelicula
pelicula = pelicula("Spider-Man: No Way Home", "Jon Watts", 148, "Acción", "B", 2021)

# Crea una instancia de la clase Espectador
espectador = espectador("Juan", "Pérez", "123456789", "2000-01-01", "311 666 6666", "juan.perez@example.com")

# Crea una instancia de la clase Proyeccion
proyeccion = Proyeccion(1, "19:00", 1, 1, 30000)

# Guarda las instancias de las clases en la base de datos
conexion.insert("pelicula", (pelicula.titulo, pelicula.director, pelicula.duracion, pelicula.genero, pelicula.clasificacion, pelicula.añoProducción))
conexion.insert("espectador", (espectador.nombre, espectador.apellido, espectador.dni, espectador.fechaNacimiento, espectador.telefono, espectador.correoElectronico))
conexion.insert("proyeccion", (proyeccion.sala, proyeccion.horaInicio, proyeccion.cine, proyeccion.espectador, proyeccion.precio))

# Cierra la conexión
conexion.cerrar()
