from django.shortcuts import render
from .models import *


# Create your views here.
def pagCategorias(request):
    return render(request, 'Categorias.html')

def pagGorras(request):
    return render(request, 'gorras.html')

def pagInfoproducto(request):
    return render(request, 'infoproductos.html')
    
def pagBuzo(request):
    return render(request, 'buzo.html')
    
