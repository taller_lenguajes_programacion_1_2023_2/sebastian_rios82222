from django.shortcuts import render, redirect
from .models import Cliente

# Create your views here.

def pagInicio(request):
    return render(request, 'index.html')

def pagLogin(request):
    mensajeError = ""
    if request.method == 'POST':
        usuario = request.POST['usuario']
        clave = request.POST['clave' ]
        
        try:
            usuarioEncontrado = Cliente.objects.get(USUARIO = usuario)
            if usuarioEncontrado.CLAVE == clave:
                return redirect('/Categorias/')
            else :
                mensajeError = "Error: calve incorrecta." 
        except:
            mensajeError = "Error: usuario no encontrado."
        
    return render(request, 'login.html', {
        "error": mensajeError
    })

def pagRegistro(request):
    if request.method == 'POST':
        nombre = request.POST[ 'nombre']
        papellido = request.POST[ 'papellido']
        sapellido = request.POST[ 'sapellido']
        correo = request.POST[ 'correo']
        usuario = request.POST[ 'usuario']
        clave = request.POST[ 'clave']
        
        nuevocliente = Cliente.objects.create(NOMBRE = nombre, P_APELLIDO = papellido, S_APELLIDO = sapellido, CORREO = correo, USUARIO = usuario, CLAVE = clave) 
        return redirect('/login/')
         
    return render(request, 'registro.html')



    


