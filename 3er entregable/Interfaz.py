import tkinter as tk
from tkinter import messagebox, ttk
from doctor import Doctor


class Ejecucion:
    def __init__(self):
        self.ventanaPrincipal = tk.Tk()
        self.ventanaPrincipal.title("Base de datos doctores")
        self.ventanaPrincipal.resizable(False, False)
        #self.ventana.iconbitmap("C:/sebastian_rios82222/3er entregable/doctor_avatar_medical_icon_140443.png")
        
        self.frameDatosDoctor = tk.Frame(self.ventanaPrincipal)
        self.frameDatosDoctor.grid(row=0, column=0, padx=5, pady=5)
        
        self.frameBotonesSuperior = tk.Frame(self.ventanaPrincipal)
        self.frameBotonesSuperior.grid(row=1, column=0, padx=5, pady=5)
        
        self.frameBaseDeDatos = tk.Frame(self.ventanaPrincipal)
        self.frameBaseDeDatos.grid(row=2, column=0, padx=5, pady=5)
        
        self.frameBotonesLateral = tk.Frame(self.ventanaPrincipal)
        self.frameBotonesLateral.grid(row=2, column=1, padx=5, pady=5)
        
        self.doctor = Doctor()  
        self.camposDeTexto()
        self.botonesArriba()
        self.crearTabla()
        self.vincularDB()
        self.botonesLateral()
       
     
        self.ventanaPrincipal.mainloop()


    def crearTabla(self):
        """Crea la tabla de visualización de datos de doctores en la interfaz."""
        self.tablaBaseDeDatos = ttk.Treeview(self.frameBaseDeDatos, show="headings")
        self.tablaBaseDeDatos.config(columns=("ID", "Nombre del Doctor", "Especialidad"))
        self.tablaBaseDeDatos.heading("ID", text="ID")
        self.tablaBaseDeDatos.heading("Nombre del Doctor", text="NOMBRE DEL DOCTOR")
        self.tablaBaseDeDatos.heading("Especialidad", text="ESPECIALIDAD")
        self.tablaBaseDeDatos.grid(row=0, column=0)
        
    def camposDeTexto(self):
        """Crea los campos de entrada para el nombre del doctor y la especialidad en la interfaz."""
        self.variableNombreDoctor = tk.StringVar()
        self.textoNombreDoctor = tk.Label(self.frameDatosDoctor, text="Nombre del Doctor: ")
        self.textoNombreDoctor.grid(row=0, column=0)
        self.cuadroNombreDoctor = tk.Entry(self.frameDatosDoctor, textvariable=self.variableNombreDoctor)
        self.cuadroNombreDoctor.grid(row=0, column=1)

        self.variableEspecialidad = tk.StringVar()
        self.textoEspecialidad = tk.Label(self.frameDatosDoctor, text="Especialidad: ")
        self.textoEspecialidad.grid(row=1, column=0)
        self.cuadroEspecialidad = tk.Entry(self.frameDatosDoctor, textvariable=self.variableEspecialidad)
        self.cuadroEspecialidad.grid(row=1, column=1)
        
    def botonesArriba(self):
        """Crea los botones superiores: Nuevo, Guardar y Cancelar."""
        self.botonNuevo = tk.Button(self.frameBotonesSuperior, text="Nuevo", command=self.Nuevo)
        self.botonNuevo.grid(row=0, column=0, padx=5)

        self.botonGuardar = tk.Button(self.frameBotonesSuperior, text="Guardar", command=self.Guardar)
        self.botonGuardar.grid(row=0, column=1, padx=5)

        self.botonCancelar = tk.Button(self.frameBotonesSuperior, text="Cancelar", command=self.Cancelar)
        self.botonCancelar.grid(row=0, column=2, padx=5)

    def Nuevo(self):
        """Maneja la acción de crear un nuevo doctor en la base de datos o borrar los campos de entrada."""
        self.variableNombreDoctor.set('')
        self.variableEspecialidad.set('')
        self.variableID.set(0)  # Nota: Asegúrate de que variableID esté definida

    def Cancelar(self):
        """Cancela la edición de un doctor y desactiva los campos de entrada."""
        self.variableNombreDoctor.set('')
        self.variableEspecialidad.set('')
        self.variableID.set(0)


    def Guardar(self):
        """
        Guarda los datos de un nuevo doctor en la base de datos o actualiza un doctor existente.
        También desactiva los campos de entrada.
        """
        id_doctor = self.variableID.get()
        if not id_doctor:
            self.doctor.Nombre = self.variableNombreDoctor.get()
          
            self.doctor.especialidad = self.variableEspecialidad.get()
            self.doctor.agregarDato()

            self.variableNombreDoctor.set('')
            self.variableEspecialidad.set('')
        else:
            self.doctor.Nombre = self.variableNombreDoctor.get()
            self.doctor.especialidad = self.variableEspecialidad.get()
            self.doctor.actualizarDatos("Doctor", "nombre", self.doctor.Nombre, id_doctor)
            self.doctor.actualizarDatos("Doctor", "especialidad", self.doctor.especialidad, id_doctor)
        self.vincularDB()

        self.variableID.set(0)

    def vincularDB(self):
        """Actualiza la tabla con los datos de doctores de la base de datos."""
        self.doctor.verTabla()
        self.tablaBaseDeDatos.delete(*self.tablaBaseDeDatos.get_children())
        for fila in self.doctor.datosEnLaTabla:
            self.tablaBaseDeDatos.insert("", "end", values=fila)

    def botonesLateral(self):
        """Crea los campos de entrada de ID y los botones Editar y Eliminar en el lateral de la interfaz."""
        self.variableID = tk.IntVar()
        self.textoID = tk.Label(self.frameBotonesLateral, text="ID del Doctor: ")
        self.textoID.grid(row=0, column=0)
        self.cuadroID = tk.Entry(self.frameBotonesLateral, textvariable=self.variableID)
        self.cuadroID.grid(row=1, column=0)

        self.botonEditar = tk.Button(self.frameBotonesLateral, text="Editar Doctor", command=self.EditarDoctor)
        self.botonEditar.grid(row=2, column=0, pady=5)

        self.botonEliminar = tk.Button(self.frameBotonesLateral, text="Eliminar Doctor", command=self.EliminarDoctor)
        self.botonEliminar.grid(row=3, column=0, pady=5)

    def EditarDoctor(self):
        """Maneja la edición de un doctor y habilita los campos de entrada para edición."""
        id_doctor = self.variableID.get()
        if id_doctor:
            doctor_data = self.doctor.obtenerDatoPorID(id_doctor)
            if doctor_data:
                self.variableNombreDoctor.set(doctor_data[1])
                self.variableEspecialidad.set(doctor_data[2])

        else:
            messagebox.showerror("Error", "Selecciona un doctor para editar.")

    def EliminarDoctor(self):
        """Elimina un doctor de la base de datos y reorganiza los IDs de doctores restantes."""
        id_doctor = self.variableID.get()
        if id_doctor is not None and id_doctor != "":
            self.doctor.eliminarDatos("Doctor", int(id_doctor))
            self.doctor.ajustarIDs(int(id_doctor))
            messagebox.showinfo("Base de datos doctores", "Se eliminaron los datos en la tabla.")
            self.vincularDB()
            self.variableID.set(0)
        else:
            messagebox.showerror("Error", "ID de doctor no válido.")
                

aplicacion = Ejecucion()
