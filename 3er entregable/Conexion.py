import sqlite3
import json

class Conexion:
    def __init__(self):
        self.baseDeDatos = sqlite3.connect("./3er entregable/base_de_datos.sqlite")
        self.apuntador = self.baseDeDatos.cursor()
        with open("C:\sebastian_rios82222/3er entregable\Queryes.json", "r") as queries:
            self.query = json.load(queries)

    def crearTabla(self, nombre, columnas):
        queryCrearTabla = self.query["CrearTabla"]
        self.apuntador.execute(queryCrearTabla)
        self.baseDeDatos.commit()
        

    def insertarDato(self, tabla, datos):
        queryInsertarDatos = self.query["InsertarDatos"].format(tabla, "?,?")
        self.apuntador.executemany(queryInsertarDatos, datos)
        self.baseDeDatos.commit()

    def seleccionarTabla(self, tabla):
        querySeleccionarTabla = self.query["SeleccionarTodo"].format(tabla)
        self.apuntador.execute(querySeleccionarTabla)
        contenido = self.apuntador.fetchall()
        return contenido

    def actualizarDatos(self, tabla, columna, nuevovalor, id):
        queryActualizarDatos = f"UPDATE {tabla} SET {columna} = ? WHERE ID = ?"
        self.apuntador.execute(queryActualizarDatos, (nuevovalor, id))
        self.baseDeDatos.commit()

    def eliminarDatos(self, tabla, id):
        queryEliminarDatos = self.query["EliminarDatos"].format(tabla, 'ID')
        self.apuntador.execute(queryEliminarDatos, (id,))
        self.baseDeDatos.commit()
