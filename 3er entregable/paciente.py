from Conexion import Conexion

class Paciente(Conexion):
    def __init__(self, Nombre_Paciente="", ID_Paciente="", ID_Doctor=""):
        super().__init__()
        self.Nombre_Paciente = Nombre_Paciente
        self.ID_Paciente = ID_Paciente
        self.ID_Doctor = ID_Doctor
        self.crearTabla("Paciente", self.query["CrearTabla"])
        self.datosEnLaTabla = []

    def agregarDato(self):
        query = self.query["InsertarDatos"]
        datos = [(self.Nombre_Paciente, self.ID_Paciente, self.ID_Doctor)]
        self.insertarDato("Paciente", datos)

    def obtenerDatoPorID(self, id_paciente):
        query = "SELECT * FROM Paciente WHERE ID_Paciente = ?"
        self.apuntador.execute(query, (id_paciente,))
        datos = self.apuntador.fetchone()
        return datos

    def verTabla(self):
        query = self.query["SeleccionarTodo"]
        self.apuntador.execute(query)
        self.datosEnLaTabla = self.apuntador.fetchall()

    def eliminarDato(self, id_paciente):
        query = self.query["EliminarDatos"]
        self.eliminarDatos(query, id_paciente)
        # Puedes ajustar los IDs después de eliminar un registro si es necesario
