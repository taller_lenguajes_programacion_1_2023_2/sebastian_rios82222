from Conexion import Conexion
import json

class Doctor(Conexion):
    def __init__(self, Nombre="", ID_Doctor="", especialidad=""):
        super().__init__()
        self.Nombre = Nombre
        self.ID_Doctor = ID_Doctor
        self.especialidad = especialidad
        self.crearTabla("Doctor",self.query ["CrearTabla"])
        self.datosEnLaTabla = []
        

    def agregarDato(self):
        query = self.query["InsertarDatos"]
      
        datos = [(self.Nombre, self.especialidad)]
        self.insertarDato("Doctor", datos)

    def obtenerDatoPorID(self, id_doctor):
        query = "SELECT * FROM Doctor WHERE ID = ?"
        self.apuntador.execute(query, (id_doctor,))
        datos = self.apuntador.fetchone()
        return datos

    def ajustarIDs(self, id_eliminado):
        # Puedes ajustar los IDs aquí si es necesario después de eliminar un registro
        pass

    def verTabla(self):
        query = self.query["SeleccionarTodo"]
        self.apuntador.execute(query)
        self.datosEnLaTabla = self.apuntador.fetchall()

    def eliminarDato(self, id_doctor):
        query = self.query["EliminarDatos"]
        self.eliminarDatos(query, id_doctor)
        # Puedes ajustar los IDs después de eliminar un registro si es necesario



