from tkinter import *
from tkinter.filedialog import *
import sv_ttk

class Aplicacion:
    def __init__(self):
        self.ventana=Tk()
        self.ventana.title("Películas")
        self.ventana.iconbitmap("Tkinter/Imagenes/LogoIcono.ico")
        self.ventana.resizable(False,False)
        self.frameDatosPelicula=Frame(self.ventana)
        self.frameDatosPelicula.grid(row=0, column=0, pady=10, padx=10)
        self.frameBotonesPelicula = Frame(self.ventana)
        self.frameBotonesPelicula.grid(row=1, column=0, pady=10, padx=10)
        self.frameTablaPeliculas = Frame(self.ventana)
        self.frameTablaPeliculas.grid(row=2, column=0, pady=10, padx=10)
        self.frameBotonesTabla = Frame(self.ventana)
        self.frameBotonesTabla.grid(row=3, column=0, pady=10, padx=10)
        self.barraDeMenu()
        self.datosPelicula()
        self.botonesPelicula()
        self.tablaPeliculas()
        self.botonesTabla()
        sv_ttk.use_dark_theme()
        
    def mostrarVentana(self):
        self.ventana.mainloop()
        
    def barraDeMenu(self):
        menuNavegacion = Menu()   
        subMenuInicio = Menu(tearoff=0)
        subMenuConsultas = Menu(tearoff=0)
        subMenuAjustes = Menu (tearoff=0)
        subMenuAyuda = Menu(tearoff=0)
        menuNavegacion.add_cascade(label='Inicio',menu=subMenuInicio)
        menuNavegacion.add_cascade(label='Consultas',menu=subMenuConsultas)
        menuNavegacion.add_cascade(label='Ajustes',menu=subMenuAjustes)
        menuNavegacion.add_cascade(label='Ayuda',menu=subMenuAyuda)
        self.ventana.config(menu=menuNavegacion) 
        
    def datosPelicula(self):
        self.labelNombre = Label(self.frameDatosPelicula, text="Nombre:")
        self.labelNombre.config(font=("Arial",12,"bold"))
        self.labelNombre.grid(row=0, column=0)
        
        self.varNombre = StringVar()
        self.cuadroNombre = Entry (self.frameDatosPelicula, textvariable=self.varNombre)
        self.cuadroNombre.config(bd=5, relief="ridge", background="#606060", font=("Arial", 12))
        self.cuadroNombre.grid(row=0, column=1, padx=5, pady=5)
        
        self.labelDuracion = Label(self.frameDatosPelicula, text="Duración:")
        self.labelDuracion.config(font=("Arial",12,"bold"))
        self.labelDuracion.grid(row=1, column=0)
        
        self.varDuracion = StringVar()
        self.cuadroDuracion = Entry (self.frameDatosPelicula, textvariable=self.varDuracion)
        self.cuadroDuracion.config(bd=5, relief="ridge", background="#606060", font=("Arial", 12))
        self.cuadroDuracion.grid(row=1, column=1, padx=5, pady=5)
        
        self.labelGenero = Label(self.frameDatosPelicula, text="Género:")
        self.labelGenero.config(font=("Arial",12,"bold"))
        self.labelGenero.grid(row=2, column=0)
        
        self.varGenero = StringVar()
        self.cuadroGenero = Entry (self.frameDatosPelicula, textvariable=self.varGenero)
        self.cuadroGenero.config(bd=5, relief="ridge", background="#606060", font=("Arial", 12))
        self.cuadroGenero.grid(row=2, column=1, padx=5, pady=5)
        
    def botonesPelicula(self):
        self.botonNuevo = Button(self.frameBotonesPelicula, text="Nuevo", command=self.habilitarCampos)
        self.botonNuevo.config(relief="raised", bd=5, bg="#d35631", font=("Arial", 10, "bold"))
        self.botonNuevo.grid(row=0, column=0,padx=10)
        
        self.botonGuardar = Button(self.frameBotonesPelicula, text="Guardar", command=self.guardarDatos)
        self.botonGuardar.config(relief="raised", bd=5, bg="#d35631", font=("Arial", 10, "bold"))
        self.botonGuardar.grid(row=0, column=1,padx=10)
        
        self.botonCancelar = Button(self.frameBotonesPelicula, text="Cancelar", command=self.deshabilitarCampos)
        self.botonCancelar.config(relief="raised", bd=5, bg="#d35631", font=("Arial", 10, "bold"))
        self.botonCancelar.grid(row=0, column=2,padx=10)
        
    def habilitarCampos(self):
        self.varNombre.set('')
        self.varDuracion.set('')
        self.varGenero.set('')       
        self.cuadroNombre.config(state='normal')
        self.cuadroDuracion.config(state='normal')
        self.cuadroGenero.config(state='normal')     
        self.botonGuardar.config(state='normal')
        self.botonCancelar.config(state='normal')
        
    def deshabilitarCampos(self):
        self.varNombre.set('')
        self.varDuracion.set('')
        self.varGenero.set('')       
        self.cuadroNombre.config(state='disabled')
        self.cuadroDuracion.config(state='disabled')
        self.cuadroGenero.config(state='disabled')     
        self.botonGuardar.config(state='disabled')
        self.botonCancelar.config(state='disabled')

    def guardarDatos(self):
        pass
    
    def tablaPeliculas(self):
        pass
    
    def botonesTabla(self):
        self.botonEditar = Button(self.frameBotonesTabla, text="Editar", command=self.editarDatos)
        self.botonEditar.config(relief="raised", bd=5, bg="#d35631", font=("Arial", 10, "bold"))
        self.botonEditar.grid(row=0, column=0,padx=10)
        
        self.botonEliminar = Button(self.frameBotonesTabla, text="Eliminar", command=self.eliminarDatos)
        self.botonEliminar.config(relief="raised", bd=5, bg="#d35631", font=("Arial", 10, "bold"))
        self.botonEliminar.grid(row=0, column=1,padx=10)
    
    def editarDatos(self):
        pass
    
    def eliminarDatos(self):
        pass