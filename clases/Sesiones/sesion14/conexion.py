import sqlite3

class Conexion:
    def __init__(self):
        self.ruta_db = "./static/db/peliculas.db"
        self.conexion = sqlite3.connect(self.ruta_db)
        self.cursor = self.conexion.cursor()
        
    def cerrar(self):
        self.conexion.commit()
        self.conexion.close()
        
cnx = Conexion()
