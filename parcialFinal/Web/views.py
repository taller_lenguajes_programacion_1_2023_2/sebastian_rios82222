from django.shortcuts import render, redirect
from .models import clientes

# Create your views here.


def pagiInicio(request):
    return render(request, 'index.html')

def pagiLogin(request):
    return render(request, 'login.html')

def pagiRegistro(request):
    if request.method == 'POST':
        nombre = request.POST['nombre']
        apellido = request.POST['apellido']
        correo = request.POST['correo']
        usuario = request.POST['usuario']
        password = request.POST['password']
        
        nuevoClientes = clientes.objects.create(NOMBRE = nombre, APELLIDO = apellido, CORREO = correo, USUARIO = usuario, CONTRASEÑA = password)
        return redirect('login/')
    return render(request, 'Registro.html')




    
