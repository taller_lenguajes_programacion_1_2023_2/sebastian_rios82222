"""
URL configuration for AppPortal project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Web.views import pagiInicio,pagiRegistro,pagiLogin
from producto.views import pagTeclados, pagVermass,pagTclado2,pagTclado3,pagTclado4,pagTclado5,pagTclado6,pagTclado7,pagTclado8,pagiCarrito


urlpatterns = [
    
    path('admin/', admin.site.urls),
    path('', pagiInicio),
    path('login/', pagiLogin),
    path('Registro/', pagiRegistro),
    path('ventas/', pagTeclados),
    path('vermas/', pagVermass),
    path('teclado2/', pagTclado2),
    path('teclado3/', pagTclado3),
    path('teclado4/', pagTclado4),
    path('teclado5/', pagTclado5),
    path('teclado6/', pagTclado6),
    path('teclado7/', pagTclado7),
    path('teclado8/', pagTclado8),
    path('carrito/', pagiCarrito),
    
]
