#peraciones basicas:sum(), mean(), max(), min() 
import statistics
numbers = [5, 10, 2, 8, 15]
total_sum = sum(numbers)
print("Suma:", total_sum)
maximum_value = max(numbers)
print("Máximo:", maximum_value)
minimum_value = min(numbers)
print("Mínimo:", minimum_value)
average = statistics.mean(numbers)
print("Promedio:", average)
