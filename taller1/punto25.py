#uso de iloc y loc 
import pandas as pd

data = {'Nombre': ['paulo', 'Maria jose', 'sebastian', 'felipe '],
        'Edad': [25, 30, 22, 28],
        'Ciudad': ['Medellin', 'Madrid', 'la ceja', 'rionegr']}

df = pd.DataFrame(data)

# iloc
row_1 = df.iloc[0]
print("Primera fila usando iloc:")
print(row_1)

subset = df.iloc[:2, :2]
print("\nSubset usando iloc:")
print(subset)

#loc
row_maria = df.loc[df['Nombre'] == 'María']
print("\nFila de María usando loc:")
print(row_maria)

subset_loc = df.loc[df['Edad'] > 25, ['Nombre', 'Edad']]
print("\nSubset usando loc:")
print(subset_loc)
