#modos de apertura 

#"r"- Leer - Valor predeterminado. Abre un archivo para leer, error si el archivo no existe

#"a"- Agregar: abre un archivo para agregarlo y crea el archivo si no existe.

#"w"- Escribir: abre un archivo para escribir, crea el archivo si no existe.

#"x"- Crear: crea el archivo especificado, devuelve un error si el archivo existe.