import json
import pandas as pd

ruta_archivo_json = 'datos.json'
with open(ruta_archivo_json) as archivo_json:
    datos_json = json.load(archivo_json)
lista_de_diccionarios = datos_json['datos']
datos = pd.DataFrame(lista_de_diccionarios)
