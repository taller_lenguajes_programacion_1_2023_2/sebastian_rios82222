import tkinter as tk 
import sv_ttk 
from tkinter import messagebox

class Interfaz():
    def __init__ (self):
        self.ventana=tk.Tk()
        self.ventana.title("Prueba")
        self.ventana.resizable(False,False)
        sv_ttk.use_dark_theme()
        self.campoTexto()
        self.menu()
        self.ventana.mainloop()
        self.edad.pack()
        
        
        
    def campoTexto(self):#                                                      nombre fuente | tamaño | estilo
        #label
        self.label_Ingrese = tk.Label(self.ventana,text="Ingrese su edad:",font=(   "arial"       ,14,   "bold"))
        self.label_Ingrese.grid(row=0,column=0,padx=20,pady=5)
        
        #imputs
        self.varEdad = tk.IntVar()
        self.input_edad = tk.Entry(self.ventana,textvariable=self.varEdad,background="grey",width=50)
        self.input_edad.grid(row=1,column=0,padx=20,pady=5)
        
        #boton
        self.boton_aceptar = tk.Button(self.ventana,text="Aceptar",command=self.validarEdad)
        self.boton_aceptar.grid(row=2,column=0,padx=20,pady=5)


    def menu(self):
        self.varMenu = tk.Menu(tearoff=0)
        self.menuAceptar = tk.Menu(tearoff=0)
        
        
        self.varMenu.add_cascade(label="Archivo",menu=self.menuAceptar)
        self.menuAceptar.add_command(label="Salir",command=self.validarEdad)
        self.ventana.config(menu=self.varMenu)



    def validarEdad(self):
        if self.varEdad.get()>=18:
            messagebox.showinfo("Alerta","Usted es mayor de edad")
        else:
            messagebox.showinfo("Alerta","Usted es menor de edad")
    
def main():
    aplicacion = Interfaz()
    
main()
